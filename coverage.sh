#!/bin/sh
set -e
lcov -q --capture --initial --base-directory . --directory . -o coverage.base
lcov -q --capture --base-directory . --directory . -o coverage.run
lcov -q --add-tracefile coverage.base --add-tracefile coverage.run -o coverage.unfiltered
lcov --remove coverage.unfiltered "/usr*" "*/test/*" -o coverage.total
lcov --summary coverage.total
