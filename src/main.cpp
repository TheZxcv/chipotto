#include <iostream>

#include "chip8.h"

uint8_t program[] = {
	0x60, 0x00, // LD V0, 0
	0x61, 0x00, // LD V1, 0
	0xF0, 0x29, // LD F, V0
	0xD0, 0x05, // DRW V0, V1, 5
};

int main(int argc, char *argv[]) {
if (argc > 1) {
	chip8 c;
	c.init();
	c.load_game(argv[1]);
		while (c.running) {
			c.cycle();
			c.keyboard[1] = chip8::KEY_DOWN;
			if (c.needs_redrawing) {
				for (int i = 0; i < chip8::SCREEN_HEIGHT; i++) {
					for (int j = 0; j < chip8::SCREEN_WIDTH; j++)
						std::cerr << (c.screen[i][j] ? "X" : ".");
					std::cerr << std::endl;
				}
				getchar();
			}
		}
	}
	return 0;
}
