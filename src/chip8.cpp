#include "chip8.h"

#include <fstream>
#include <iostream>

#include "common.h"

typedef void (chip8::*instruction_t)(opcode_t);

constexpr uint8_t get_regX(opcode_t op) {
	return (op & 0x0f00) >> 8;
}

constexpr uint8_t get_regY(opcode_t op) {
	return (op & 0x00f0) >> 4;
}

constexpr uint8_t get_imm4(opcode_t op) {
	return op & 0x000f;
}

constexpr uint8_t get_imm8(opcode_t op) {
	return op & 0x00ff;
}

constexpr uint16_t get_imm12(opcode_t op) {
	return op & 0x0fff;
}


chip8::chip8(const Audio * const audio) : audio_output(audio){
	init();
}

void chip8::init() {
	I = 0;
	DT = 0;
	ST = 0;
	SP = 0;
	PC = ENTRY_POINT;
	std::fill(std::begin(V), std::end(V), 0);
	std::fill(std::begin(memory), std::end(memory), 0);
	cls(0);

	running = true;
	needs_redrawing = false;
	waiting_for_key = false;

	std::fill(std::begin(keyboard), std::end(keyboard), chip8::KEY_UP);

	load_digit_sprites();
	populate_lookuptable();
}

void chip8::load_game(const std::string &filename) {
	ssize_t filesize = get_file_size(filename);
	if (filesize == -1) {
		std::cerr << "Error while opening ROM file!" << std::endl;
		std::exit(-1);
	} else if (((size_t) filesize) > PROGRAM_MAXSIZE) {
		std::cerr << "ROM file too big: ";
		std::cerr << filesize << " bytes" << std::endl;
		std::exit(-1);
	} else {
		std::ifstream file(filename, std::ios::binary);
		file.read((char *) &memory[ENTRY_POINT], filesize);
	}
}

void chip8::cycle() {
	if (needs_redrawing)
		return;

	if (running && !waiting_for_key) {
		uint16_t oldPC = PC;

		update_timer();
		update_sound();
		opcode_t opcode = fetch();
		instruction_t instr = decode(opcode);
		((*this).*instr)(opcode);

		// (possibly) infinite loop
		if (oldPC == PC)
			running = false;
	}
}

void chip8::increment_PC() {
	PC += 2;
}

void chip8::update_timer() {
	if (DT > 0)
		DT--;
}

void chip8::update_sound() {
	if (ST > 0) {
		if (audio_output)
			audio_output->beep();
		ST--;
	}
}

void chip8::redrawn() {
	needs_redrawing = false;
}

void chip8::press_key(uint8_t key) {
	if (waiting_for_key) {
		V[pressed_key_reg] = key;
		waiting_for_key = false;
	}
	keyboard[key] = KEY_DOWN;
}

void chip8::release_key(uint8_t key) {
	keyboard[key] = KEY_UP;
}

opcode_t chip8::fetch() {
	opcode_t op = (memory[PC] << 8) | memory[PC + 1];
	increment_PC();
	return op;
}

instruction_t chip8::decode(opcode_t opcode) const {
	return lookup_table[opcode];
}

void chip8::invalid(opcode_t op) {
	std::cerr << "Invalid opcode: " << std::hex << op << std::endl;
	std::exit(-1);
}

void chip8::sys(opcode_t op) {
	(void) op;
	/* ignored */
}

void chip8::cls(opcode_t op) {
	(void) op;
	for (auto &i : screen)
		std::fill(std::begin(i), std::end(i), false);
}

void chip8::ret(opcode_t op) {
	(void) op;
	PC = stack[SP--];
}

void chip8::jp(opcode_t op) {
	const uint16_t addr = get_imm12(op);
	PC = addr;
}

void chip8::call(opcode_t op) {
	const uint16_t addr = get_imm12(op);
	stack[++SP] = PC;
	PC = addr;
}

void chip8::se_imm8(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t byte = get_imm8(op);
	if (V[regX] == byte)
		increment_PC();
}

void chip8::sne_imm8(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t byte = get_imm8(op);
	if (V[regX] != byte)
		increment_PC();
}

void chip8::se(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	if (V[regX] == V[regY])
		increment_PC();
}

void chip8::ld_imm8(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t byte = get_imm8(op);

	V[regX] = byte;
}

void chip8::add_imm8(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t byte = get_imm8(op);

	V[regX] += byte;
}

void chip8::ld(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	V[regX] = V[regY];
}

void chip8::_or(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	V[regX] |= V[regY];
}

void chip8::_and(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	V[regX] &= V[regY];
}

void chip8::_xor(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	V[regX] ^= V[regY];
}

void chip8::add(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	int res = V[regX] + V[regY];
	if (res > 0xff)
		V[VF] = 1;
	else
		V[VF] = 0;
	V[regX] = (uint8_t) (res & 0xff);
}

void chip8::sub(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	if (V[regX] >= V[regY])
		V[VF] = 1;
	else
		V[VF] = 0;
	int res = V[regX] - V[regY];
	V[regX] = (uint8_t) (res & 0xff);
}

void chip8::shr(opcode_t op) {
	const uint8_t regX = get_regX(op);

	V[VF] = V[regX] & 0x01;
	V[regX] >>= 1;
}

void chip8::subn(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	if (V[regY] >= V[regX])
		V[VF] = 1;
	else
		V[VF] = 0;
	int res = V[regY] - V[regX];
	V[regX] = (uint8_t) (res & 0xff);
}

void chip8::shl(opcode_t op) {
	const uint8_t regX = get_regX(op);

	V[VF] = (V[regX] & 0x80) ? 1 : 0;
	V[regX] <<= 1;
}

void chip8::sne(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);

	if (V[regX] != V[regY])
		increment_PC();
}

void chip8::ld_I(opcode_t op) {
	const uint16_t addr = get_imm12(op);
	I = addr;
}

void chip8::jp_reg(opcode_t op) {
	const uint16_t offset = op & 0x0fff;
	PC = V[0] + offset;
}

void chip8::rnd(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t byte = get_imm8(op);

	// TODO: use better randomizer
	V[regX] = ((uint8_t) (rand() % 256)) & byte;
}

void chip8::drw(opcode_t op) {
	const uint8_t regX = get_regX(op);
	const uint8_t regY = get_regY(op);
	const uint8_t nbytes = get_imm4(op);

	uint8_t x = V[regX];
	uint8_t y = V[regY];

	V[VF] = 0x00;
	for (uint16_t i = 0; i < nbytes; i++) {
		uint8_t byte = memory[I + i];
		for (uint8_t j = 0, mask = 0x80; j < 8; j++, mask >>= 1) {
			const bool pixel = (byte & mask) != 0;
			const uint16_t posY = (y + i) % SCREEN_HEIGHT;
			const uint16_t posX = (x + j) % SCREEN_WIDTH;

			// collision
			if (screen[posY][posX] && pixel)
				V[VF] = 0x01;

			screen[posY][posX] ^= pixel;
		}
	}
	needs_redrawing = true;
}

void chip8::skp(opcode_t op) {
	const uint8_t regX = get_regX(op);
	if (keyboard[V[regX]] == chip8::KEY_DOWN)
		increment_PC();
}

void chip8::sknp(opcode_t op) {
	const uint8_t regX = get_regX(op);
	if (keyboard[V[regX]] == chip8::KEY_UP)
		increment_PC();
}

void chip8::ld_reg_DT(opcode_t op) {
	const uint8_t regX = get_regX(op);
	V[regX] = DT;
}

void chip8::ld_reg_key(opcode_t op) {
	const uint8_t regX = get_regX(op);
	waiting_for_key = true;
	pressed_key_reg = regX;
}

void chip8::ld_DT_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	DT = V[regX];
}

void chip8::ld_ST_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	ST = V[regX];
}

void chip8::add_I_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	I += V[regX];
	V[VF] = (I > 0x0fff) ? 1 : 0;
}

void chip8::ld_F_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	// load location of digit representation of V[regX] into I
	I = V[regX] * 5;
}

void chip8::ld_B_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	// load location of BCD representation of V[regX] into I, I+1 and I+2
	uint8_t value = V[regX];
	memory[I]     = (value / 100) % 10;
	memory[I + 1] = (value / 10) % 10;
	memory[I + 2] = value % 10;
}

void chip8::ld_addrI_reg(opcode_t op) {
	const uint8_t regX = get_regX(op);
	for (int i = 0; i <= regX; i++)
		memory[I + i] = V[i];
	I += regX + 1;
}

void chip8::ld_reg_addrI(opcode_t op) {
	const uint8_t regX = get_regX(op);
	for (int i = 0; i <= regX; i++)
		V[i] = memory[I + i];
	I += regX + 1;
}

void chip8::populate_lookuptable() {
	std::fill(std::begin(lookup_table), std::end(lookup_table), &chip8::invalid);
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[i] = &chip8::sys;
	lookup_table[0x00e0] = &chip8::cls;
	lookup_table[0x00ee] = &chip8::ret;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x1000 | i] = &chip8::jp;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x2000 | i] = &chip8::call;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x3000 | i] = &chip8::se_imm8;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x4000 | i] = &chip8::sne_imm8;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x5000 | (i << 4)] = &chip8::se;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x6000 | i] = &chip8::ld_imm8;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0x7000 | i] = &chip8::add_imm8;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8000 | (i << 4)] = &chip8::ld;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8001 | (i << 4)] = &chip8::_or;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8002 | (i << 4)] = &chip8::_and;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8003 | (i << 4)] = &chip8::_xor;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8004 | (i << 4)] = &chip8::add;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8005 | (i << 4)] = &chip8::sub;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8006 | (i << 4)] = &chip8::shr;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x8007 | (i << 4)] = &chip8::subn;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x800e | (i << 4)] = &chip8::shl;
	for (int i = 0; i <= 0xff; i++)
		lookup_table[0x9000 | (i << 4)] = &chip8::sne;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0xA000 | i] = &chip8::ld_I;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0xB000 | i] = &chip8::jp_reg;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0xC000 | i] = &chip8::rnd;
	for (int i = 0; i <= 0xfff; i++)
		lookup_table[0xD000 | i] = &chip8::drw;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xe09e | (i << 8)] = &chip8::skp;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xe0a1 | (i << 8)] = &chip8::sknp;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf007 | (i << 8)] = &chip8::ld_reg_DT;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf00a | (i << 8)] = &chip8::ld_reg_key;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf015 | (i << 8)] = &chip8::ld_DT_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf018 | (i << 8)] = &chip8::ld_ST_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf01e | (i << 8)] = &chip8::add_I_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf029 | (i << 8)] = &chip8::ld_F_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf033 | (i << 8)] = &chip8::ld_B_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf055 | (i << 8)] = &chip8::ld_addrI_reg;
	for (int i = 0; i <= 0xf; i++)
		lookup_table[0xf065 | (i << 8)] = &chip8::ld_reg_addrI;
}

void chip8::load_digit_sprites() {
	// 0
	memory[0x000] = 0xf0;
	memory[0x001] = 0x90;
	memory[0x002] = 0x90;
	memory[0x003] = 0x90;
	memory[0x004] = 0xf0;

	// 1
	memory[0x005] = 0x20;
	memory[0x006] = 0x60;
	memory[0x007] = 0x20;
	memory[0x008] = 0x20;
	memory[0x009] = 0x70;

	// 2
	memory[0x00a] = 0xf0;
	memory[0x00b] = 0x10;
	memory[0x00c] = 0xf0;
	memory[0x00d] = 0x80;
	memory[0x00e] = 0xf0;

	// 3
	memory[0x00f] = 0xf0;
	memory[0x010] = 0x10;
	memory[0x011] = 0xf0;
	memory[0x012] = 0x10;
	memory[0x013] = 0xf0;

	// 4
	memory[0x014] = 0x90;
	memory[0x015] = 0x90;
	memory[0x016] = 0xf0;
	memory[0x017] = 0x10;
	memory[0x018] = 0x10;

	// 5
	memory[0x019] = 0xf0;
	memory[0x01a] = 0x80;
	memory[0x01b] = 0xf0;
	memory[0x01c] = 0x10;
	memory[0x01d] = 0xf0;

	// 6
	memory[0x01e] = 0xf0;
	memory[0x01f] = 0x80;
	memory[0x020] = 0xf0;
	memory[0x021] = 0x90;
	memory[0x022] = 0xf0;

	// 7
	memory[0x023] = 0xf0;
	memory[0x024] = 0x10;
	memory[0x025] = 0x20;
	memory[0x026] = 0x40;
	memory[0x027] = 0x40;

	// 8
	memory[0x028] = 0xf0;
	memory[0x029] = 0x90;
	memory[0x02a] = 0xf0;
	memory[0x02b] = 0x90;
	memory[0x02c] = 0xf0;

	// 9
	memory[0x02d] = 0xf0;
	memory[0x02e] = 0x90;
	memory[0x02f] = 0xf0;
	memory[0x030] = 0x10;
	memory[0x031] = 0xf0;

	// a
	memory[0x032] = 0xf0;
	memory[0x033] = 0x90;
	memory[0x034] = 0xf0;
	memory[0x035] = 0x90;
	memory[0x036] = 0x90;

	// b
	memory[0x037] = 0xe0;
	memory[0x038] = 0x90;
	memory[0x039] = 0xe0;
	memory[0x03a] = 0x90;
	memory[0x03b] = 0xe0;

	// c
	memory[0x03c] = 0xf0;
	memory[0x03d] = 0x80;
	memory[0x03e] = 0x80;
	memory[0x03f] = 0x80;
	memory[0x040] = 0xf0;

	// d
	memory[0x041] = 0xe0;
	memory[0x042] = 0x90;
	memory[0x043] = 0x90;
	memory[0x044] = 0x90;
	memory[0x045] = 0xe0;

	// e
	memory[0x046] = 0xf0;
	memory[0x047] = 0x80;
	memory[0x048] = 0xf0;
	memory[0x049] = 0x80;
	memory[0x04a] = 0xf0;

	// f
	memory[0x04b] = 0xf0;
	memory[0x04c] = 0x80;
	memory[0x04d] = 0xf0;
	memory[0x04e] = 0x80;
	memory[0x04f] = 0x80;
}
