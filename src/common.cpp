#include <sys/stat.h>

#include "common.h"

ssize_t get_file_size(const std::string &filename) {
	struct stat stat_buf;
	int rc = stat(filename.c_str(), &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}
