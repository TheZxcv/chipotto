#include <string>
#include <cstdlib>

ssize_t get_file_size(const std::string &filename);
