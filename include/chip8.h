#include <cstdint>
#include <functional>

#include "audio.h"

typedef uint16_t opcode_t;

class chip8 {
typedef void (chip8::*instruction_t)(opcode_t);
public:
	static const std::size_t MEMORY_SIZE       = 0xfff;
	static const uint16_t    RESERVED_MEM      = 0x000;
	static const std::size_t RESERVED_MEM_SIZE = 0x1ff;
	static const uint16_t    ENTRY_POINT       = 0x200;
	static const uint16_t    ENTRY_POINT_ETI   = 0x600;

	static const std::size_t STACK_SIZE = 16;
	static const std::size_t REGISTERS_NUM = 16;
	static const std::size_t VF = 0xf;

	static const std::size_t PROGRAM_MAXSIZE = MEMORY_SIZE - ENTRY_POINT;

	static const uint8_t SCREEN_WIDTH  = 64;
	static const uint8_t SCREEN_HEIGHT = 32;

	chip8(const Audio * const audio = nullptr);
	//~chip8();

	void init();
	void cycle();
	void load_game(const std::string &filename);
	void press_key(uint8_t key);
	void release_key(uint8_t key);
	void redrawn();

	bool running;
	bool waiting_for_key;
	bool needs_redrawing;
	bool screen[SCREEN_HEIGHT][SCREEN_WIDTH];
	enum key_state_t { KEY_UP, KEY_DOWN };
	key_state_t keyboard[256];

	uint8_t  memory[MEMORY_SIZE];
	uint8_t  V[REGISTERS_NUM];
	uint16_t I;
	uint16_t PC;
	uint8_t  SP;
	uint16_t stack[STACK_SIZE];

	uint8_t DT;
	uint8_t ST;

	void sys(opcode_t op);
	void cls(opcode_t op);
	void ret(opcode_t op);
	void jp(opcode_t op);
	void call(opcode_t op);
	void se_imm8(opcode_t op);
	void sne_imm8(opcode_t op);
	void se(opcode_t op);
	void ld_imm8(opcode_t op);
	void add_imm8(opcode_t op);
	void ld(opcode_t op);
	void _or(opcode_t op);
	void _and(opcode_t op);
	void _xor(opcode_t op);
	void add(opcode_t op);
	void sub(opcode_t op);
	void shr(opcode_t op);
	void subn(opcode_t op);
	void shl(opcode_t op);
	void sne(opcode_t op);
	void ld_I(opcode_t op);
	void jp_reg(opcode_t op);
	void rnd(opcode_t op);
	void drw(opcode_t op);
	void skp(opcode_t op);
	void sknp(opcode_t op);
	void ld_reg_DT(opcode_t op);
	void ld_reg_key(opcode_t op);
	void ld_DT_reg(opcode_t op);
	void ld_ST_reg(opcode_t op);
	void add_I_reg(opcode_t op);
	void ld_F_reg(opcode_t op);
	void ld_B_reg(opcode_t op);
	void ld_addrI_reg(opcode_t op);
	void ld_reg_addrI(opcode_t op);

private:
	const Audio * const audio_output;
	uint8_t pressed_key_reg;

	void populate_lookuptable();
	void load_digit_sprites();

	void update_timer();
	void update_sound();
	void increment_PC();

	opcode_t fetch();
	instruction_t decode(opcode_t) const;

	void invalid(opcode_t op);
	/*const static*/ instruction_t lookup_table[0xffff + 1];
};

