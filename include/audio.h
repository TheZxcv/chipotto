#ifndef AUDIO_H
#define AUDIO_H

struct Audio {
	virtual ~Audio() {}
	virtual void beep() const = 0;
};

#endif
