#include "window.h"
#include "configs.h"

#include <QPainter>
#include <QApplication>
#include <iostream>

Window::Window(QWidget *parent) :
	QWidget(parent),
	m_board(QImage(chip8::SCREEN_WIDTH, chip8::SCREEN_HEIGHT, QImage::Format_ARGB32)),
	m_audio(configs::DEFAULT_BEEP_WAV_FILE),
	m_chip(&m_audio) {

	resize(configs::WIN_SCREEN_WIDTH, configs::WIN_SCREEN_HEIGHT);
	init();
	startTimer(10);
}

void Window::init() {
	m_chip.init();
	QStringList stringList = QCoreApplication::arguments();
	if (stringList.size() > 1)
		m_chip.load_game(stringList.at(1).toStdString());
	else
		QApplication::instance()->quit();
	empty  = QColor( 15,  56,  15).rgb();
	filled = QColor(170, 170, 170).rgb();
	m_board.fill(empty);
}

void Window::paintEvent(QPaintEvent *e) {
	Q_UNUSED(e);

	QPainter qp(this);
	QImage scaled = m_board.scaled(configs::WIN_SCREEN_WIDTH, configs::WIN_SCREEN_HEIGHT);
	qp.drawImage(QPoint(0, 0), scaled);
}

void Window::timerEvent(QTimerEvent *e) {
	Q_UNUSED(e);

	int cycles = 0;
	if (!m_chip.waiting_for_key) {
		do {
			m_chip.cycle();
			cycles++;
		} while (!m_chip.needs_redrawing && !m_chip.waiting_for_key && cycles < 65536);
	}

	if (m_chip.needs_redrawing) {
		for (int i = 0; i < m_chip.SCREEN_WIDTH; i++) {
			for (int j = 0; j < m_chip.SCREEN_HEIGHT; j++) {
				QRgb color = m_chip.screen[j][i] ? filled : empty;
				m_board.setPixel(i, j, color);
			}
		}
		m_chip.redrawn();
		repaint();
	}
}

static int toKey(int keycode) {
	switch (keycode) {
		case Qt::Key_1:
			return 0;
		case Qt::Key_2:
			return 1;
		case Qt::Key_3:
			return 2;
		case Qt::Key_4:
			return 3;
		case Qt::Key_W:
			return 4;
		case Qt::Key_E:
			return 5;
		case Qt::Key_R:
			return 6;
		case Qt::Key_T:
			return 7;
		case Qt::Key_S:
			return 8;
		case Qt::Key_D:
			return 9;
		case Qt::Key_F:
			return 10;
		case Qt::Key_G:
			return 11;
		case Qt::Key_X:
			return 12;
		case Qt::Key_C:
			return 13;
		case Qt::Key_V:
			return 14;
		case Qt::Key_B:
			return 15;
		default:
			return -1;
	}
}

void Window::keyPressEvent(QKeyEvent *e) {
	int key = e->key();

	if (key == Qt::Key_Q)
		QApplication::instance()->quit();
	if (toKey(key) >= 0)
		m_chip.press_key(toKey(key));
	QWidget::keyPressEvent(e);
}

void Window::keyReleaseEvent(QKeyEvent *e) {
	int key = e->key();

	if (key == Qt::Key_Q)
		QApplication::instance()->quit();
	if (toKey(key) >= 0)
		m_chip.release_key(toKey(key));
	QWidget::keyReleaseEvent(e);
}
