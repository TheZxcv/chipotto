#include "qtaudio.h"

QtAudio::QtAudio(const char * const filename) {
	this->m_beep = new QSound(filename);
}

QtAudio::~QtAudio() {
	delete m_beep;
}

void QtAudio::beep() const {
	if (m_beep->isFinished()) {
		m_beep->play();
	}
}
