#ifndef QT_AUDIO_H
#define QT_AUDIO_H

#include "audio.h"
#include <QSound>

class QtAudio : public Audio {
public:
	QtAudio(const char * const filename);
	~QtAudio();

	void beep() const;
private:
	QSound *m_beep;
};

#endif
