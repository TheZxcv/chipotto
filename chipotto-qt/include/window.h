#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>
#include <QKeyEvent>
#include <QSound>

#include "qtaudio.h"
#include "chip8.h"

class Window : public QWidget {

public:
	explicit Window(QWidget *parent = nullptr);

protected:
	void paintEvent(QPaintEvent *);
	void timerEvent(QTimerEvent *);
	void keyPressEvent(QKeyEvent *);
	void keyReleaseEvent(QKeyEvent *);

private:
	QRgb empty;
	QRgb filled;
	QImage m_board;
	QtAudio m_audio;
	chip8 m_chip;
	void init();
};

#endif // WINDOW_H
