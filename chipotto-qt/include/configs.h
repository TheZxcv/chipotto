#ifndef QT_CHIP8_CONFIGS_H
#define QT_CHIP8_CONFIGS_H

namespace configs {
static const char * const DEFAULT_BEEP_WAV_FILE = "data/beep.wav";

static const int WIN_SCREEN_WIDTH  = 64*8;
static const int WIN_SCREEN_HEIGHT = 32*8;
}

#endif // QT_CHIP8_CONFIGS_H
