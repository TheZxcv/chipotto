# chipotto [![build status](https://gitlab.com/TheZxcv/chipotto/badges/master/build.svg)](https://gitlab.com/TheZxcv/chipotto/commits/master)[![coverage report](https://gitlab.com/TheZxcv/chipotto/badges/master/coverage.svg)](https://gitlab.com/TheZxcv/chipotto/commits/master)
Barebone emulator for Chip-8

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```
The main executable is `chipotto-qt` and can be found under `build/chipotto-qt/`.

## Usage

```bash
$ chipotto-qt [ROM]
```
