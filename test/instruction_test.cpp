#include "chip8.h"

#include <gtest/gtest.h>

namespace {

TEST(Instruction, SYS) {
	chip8 chip;
	chip.PC = 0;

	chip.sys(0x0000);
	// does nothing
	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, CLS) {
	chip8 chip;
	chip.screen[16][32] = true;

	chip.cls(0x00e0);

	for (auto &i : chip.screen)
		for (auto &j : i)
			EXPECT_FALSE(j);
}

TEST(Instruction, RET) {
	chip8 chip;
	chip.SP = 1;
	chip.stack[chip.SP] = 0x0123;
	chip.PC = 0;

	chip.ret(0x00ee);

	EXPECT_EQ(0x0123, chip.PC);
	EXPECT_EQ(0, chip.SP);
}

TEST(Instruction, JP) {
	chip8 chip;
	chip.PC = 0;
	const uint16_t nextPC = 0x0123;

	chip.jp(0x1000 | nextPC);

	EXPECT_EQ(nextPC, chip.PC);
}

TEST(Instruction, CALL) {
	chip8 chip;
	chip.PC = 0x0333;
	chip.SP = 0;
	const uint16_t nextPC = 0x0123;

	chip.call(0x2000 | nextPC);

	EXPECT_EQ(nextPC, chip.PC);
	EXPECT_EQ(1, chip.SP);
	EXPECT_EQ(0x0333, chip.stack[1]);
}

TEST(Instruction, SE_IMM8_taken) {
	chip8 chip;
	chip.V[1] = 0x12;
	chip.PC = 0;

	chip.se_imm8(0x3000 | 0x0100 | 0x0012);

	EXPECT_EQ(2, chip.PC);
}

TEST(Instruction, SE_IMM8_not_taken) {
	chip8 chip;
	chip.V[2] = 0x12;
	chip.PC = 0;

	chip.se_imm8(0x3000 | 0x0200 | 0x0034);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, SNE_IMM8_taken) {
	chip8 chip;
	chip.V[2] = 0x12;
	chip.PC = 0;

	chip.sne_imm8(0x4000 | 0x0200 | 0x0034);

	EXPECT_EQ(2, chip.PC);
}

TEST(Instruction, SNE_IMM8_not_taken) {
	chip8 chip;
	chip.V[1] = 0x12;
	chip.PC = 0;

	chip.sne_imm8(0x4000 | 0x0100 | 0x0012);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, SE_taken) {
	chip8 chip;
	chip.V[1] = 0x11;
	chip.V[3] = 0x11;
	chip.PC = 0;

	chip.se(0x5000 | 0x0100 | 0x0030);

	EXPECT_EQ(2, chip.PC);
}


TEST(Instruction, SE_not_taken) {
	chip8 chip;
	chip.V[1] = 0x11;
	chip.V[3] = 0x22;
	chip.PC = 0;

	chip.se(0x5000 | 0x0100 | 0x0030);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, LD_IMM8) {
	chip8 chip;
	chip.V[5] = 0x00;

	chip.ld_imm8(0x6000 | 0x0500 | 0x0012);

	EXPECT_EQ(0x12, chip.V[5]);
}

TEST(Instruction, ADD_IMM8) {
	chip8 chip;
	chip.V[7] = 0x01;

	chip.add_imm8(0x7000 | 0x0700 | 0x0002);

	EXPECT_EQ(0x03, chip.V[7]);
}

TEST(Instruction, LD) {
	chip8 chip;
	chip.V[0xc] = 0x00;
	chip.V[0xa] = 0xaa;

	chip.ld(0x8000 | 0x0c00 | 0x00a0);

	EXPECT_EQ(0xaa, chip.V[0xc]);
}

TEST(Instruction, OR) {
	chip8 chip;
	chip.V[8] = 0x90;
	chip.V[9] = 0x11;

	chip._or(0x8001 | 0x0800 | 0x0090);

	EXPECT_EQ(0x91, chip.V[8]);
}

TEST(Instruction, AND) {
	chip8 chip;
	chip.V[6] = 0x90;
	chip.V[7] = 0x11;

	chip._and(0x8002 | 0x0600 | 0x0070);

	EXPECT_EQ(0x10, chip.V[6]);
}

TEST(Instruction, XOR) {
	chip8 chip;
	chip.V[0] = 0xa5;
	chip.V[1] = 0x5a;

	chip._xor(0x8003 | 0x0000 | 0x0010);

	EXPECT_EQ(0xff, chip.V[0]);
}

TEST(Instruction, ADD) {
	 chip8 chip;
	 chip.V[1] = 0x03;
	 chip.V[2] = 0x01;

	 chip.add(0x8004 | 0x0100 | 0x0020);

	 EXPECT_EQ(0x04, chip.V[1]);
	 EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, ADD_overflow) {
	 chip8 chip;
	 chip.V[1] = 0xff;
	 chip.V[2] = 0x01;

	 chip.add(0x8004 | 0x0100 | 0x0020);

	 EXPECT_EQ(0x00, chip.V[1]);
	 EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SUB) {
	chip8 chip;
	chip.V[3] = 0x06;
	chip.V[4] = 0x03;

	chip.sub(0x8005 | 0x0300 | 0x0040);

	EXPECT_EQ(0x03, chip.V[3]);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SUB_equal_operands) {
	chip8 chip;
	chip.V[3] = 0x01;
	chip.V[4] = 0x01;

	chip.sub(0x8005 | 0x0300 | 0x0040);

	EXPECT_EQ(0x00, chip.V[3]);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SUB_overflow) {
	chip8 chip;
	chip.V[3] = 0x03;
	chip.V[4] = 0x06;

	chip.sub(0x8005 | 0x0300 | 0x0040);

	EXPECT_EQ(0xfd, chip.V[3]);
	EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, SHR) {
	chip8 chip;
	chip.V[0] = 0x82;

	chip.shr(0x8006 | 0x0000);

	EXPECT_EQ(0x41, chip.V[0]);
	EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, SHR_lsb) {
	chip8 chip;
	chip.V[0] = 0x83;

	chip.shr(0x8006 | 0x0000);

	EXPECT_EQ(0x41, chip.V[0]);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SUBN) {
	chip8 chip;
	chip.V[3] = 0x03;
	chip.V[4] = 0x06;

	chip.subn(0x8007 | 0x0300 | 0x0040);

	EXPECT_EQ(0x03, chip.V[3]);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SUBN_overflow) {
	chip8 chip;
	chip.V[3] = 0x06;
	chip.V[4] = 0x03;

	chip.subn(0x8007 | 0x0300 | 0x0040);

	EXPECT_EQ(0xfd, chip.V[3]);
	EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, SHL) {
	chip8 chip;
	chip.V[0] = 0x42;

	chip.shl(0x800e | 0x0000);

	EXPECT_EQ(0x84, chip.V[0]);
	EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, SHL_msb) {
	chip8 chip;
	chip.V[0] = 0xc2;

	chip.shl(0x800e | 0x0000);

	EXPECT_EQ(0x84, chip.V[0]);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, SNE_not_taken) {
	chip8 chip;
	chip.V[1] = 0x11;
	chip.V[3] = 0x11;
	chip.PC = 0;

	chip.sne(0x9000 | 0x0100 | 0x0030);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, SNE_taken) {
	chip8 chip;
	chip.V[1] = 0x11;
	chip.V[3] = 0x22;
	chip.PC = 0;

	chip.sne(0x9000 | 0x0100 | 0x0030);

	EXPECT_EQ(2, chip.PC);
}

TEST(Instruction, LD_I) {
	chip8 chip;
	chip.I = 0;

	chip.ld_I(0xA000 | 0x0123);

	EXPECT_EQ(0x0123, chip.I);
}

TEST(Instruction, JP_REG) {
	chip8 chip;
	chip.V[0] = 0x10;
	chip.PC = 0;

	chip.jp_reg(0xb000 | 0x0123);

	EXPECT_EQ(0x0123 + 0x10, chip.PC);
}

TEST(Instruction, DISABLED_RND) {
	chip8 chip;
}

TEST(Instruction, DISABLED_DRW) {
	chip8 chip;
}

TEST(Instruction, SKP_up) {
	chip8 chip;
	chip.PC = 0;
	chip.V[1] = 5;
	chip.keyboard[5] = chip8::KEY_UP;

	chip.skp(0xe09e | 0x0100);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, SKP_down) {
	chip8 chip;
	chip.PC = 0;
	chip.V[1] = 5;
	chip.keyboard[5] = chip8::KEY_DOWN;

	chip.skp(0xe09e | 0x0100);

	EXPECT_EQ(2, chip.PC);
}

TEST(Instruction, SKNP_up) {
	chip8 chip;
	chip.PC = 0;
	chip.V[1] = 5;
	chip.keyboard[5] = chip8::KEY_UP;

	chip.sknp(0xe0a1 | 0x0100);

	EXPECT_EQ(2, chip.PC);
}

TEST(Instruction, SKNP_down) {
	chip8 chip;
	chip.PC = 0;
	chip.V[1] = 5;
	chip.keyboard[5] = chip8::KEY_DOWN;

	chip.sknp(0xe0a1 | 0x0100);

	EXPECT_EQ(0, chip.PC);
}

TEST(Instruction, LD_REG_DT) {
	chip8 chip;
	chip.DT = 0x21;
	chip.V[0xb] = 0;

	chip.ld_reg_DT(0xf007 | 0x0b00);

	EXPECT_EQ(0x21, chip.V[0xb]);
}

TEST(Instruction, DISABLED_LD_REG_KEY) {
	chip8 chip;
	// 0xfx0a
}

TEST(Instruction, LD_DT_REG) {
	chip8 chip;
	chip.V[0xe] = 0xff;
	chip.DT = 0x00;

	chip.ld_DT_reg(0xf015 | 0x0e00);

	EXPECT_EQ(0xff, chip.DT);
}

TEST(Instruction, LD_ST_REG) {
	chip8 chip;
	chip.V[0] = 0xef;
	chip.ST = 0x00;

	chip.ld_ST_reg(0xf018 | 0x0000);

	EXPECT_EQ(0xef, chip.ST);
}

TEST(Instruction, ADD_I_REG) {
	chip8 chip;
	chip.V[1] = 0x03;
	chip.I = 0x01;

	chip.add_I_reg(0xf01e | 0x0100);

	EXPECT_EQ(0x04, chip.I);
	EXPECT_EQ(0, chip.V[chip.VF]);
}

TEST(Instruction, ADD_I_REG_overflow) {
	chip8 chip;
	chip.V[1] = 0x03;
	chip.I = 0x0fff;

	chip.add_I_reg(0xf01e | 0x0100);

	EXPECT_EQ(0x1002, chip.I);
	EXPECT_EQ(1, chip.V[chip.VF]);
}

TEST(Instruction, LD_F_REG) {
	chip8 chip;
	chip.V[0] = 0;

	chip.ld_F_reg(0xf029 | 0x0000);

	// sprite digit 0
	const uint16_t loc = chip.V[0];
	EXPECT_EQ(0xf0, chip.memory[loc]);
	EXPECT_EQ(0x90, chip.memory[loc + 1]);
	EXPECT_EQ(0x90, chip.memory[loc + 2]);
	EXPECT_EQ(0x90, chip.memory[loc + 3]);
	EXPECT_EQ(0xf0, chip.memory[loc + 4]);
}

TEST(Instruction, LD_B_REG) {
	chip8 chip;
	chip.V[1] = 123;
	chip.I = 0;

	chip.ld_B_reg(0xf033 | 0x0100);

	const uint16_t loc = chip.I;
	EXPECT_EQ(1, chip.memory[loc]);
	EXPECT_EQ(2, chip.memory[loc + 1]);
	EXPECT_EQ(3, chip.memory[loc + 2]);
}

TEST(Instruction, LD_ADDRI_REG) {
	chip8 chip;
	chip.V[0] = 4;
	chip.V[1] = 3;
	chip.V[2] = 2;
	chip.V[3] = 1;
	chip.V[4] = 0;
	chip.I = 0;
	const uint16_t loc = chip.I;

	chip.ld_addrI_reg(0xf055 | 0x0400);

	EXPECT_EQ(5, chip.I);
	EXPECT_EQ(4, chip.memory[loc]);
	EXPECT_EQ(3, chip.memory[loc + 1]);
	EXPECT_EQ(2, chip.memory[loc + 2]);
	EXPECT_EQ(1, chip.memory[loc + 3]);
	EXPECT_EQ(0, chip.memory[loc + 4]);
}

TEST(Instruction, LD_REG_ADDRI) {
	chip8 chip;
	chip.I = 0;
	const uint16_t loc = chip.I;
	chip.memory[loc] = 8;
	chip.memory[loc + 1] = 7;
	chip.memory[loc + 2] = 6;
	chip.memory[loc + 3] = 5;
	chip.memory[loc + 4] = 4;

	chip.ld_reg_addrI(0xf065 | 0x0400);

	EXPECT_EQ(5, chip.I);
	EXPECT_EQ(8, chip.V[0]);
	EXPECT_EQ(7, chip.V[1]);
	EXPECT_EQ(6, chip.V[2]);
	EXPECT_EQ(5, chip.V[3]);
	EXPECT_EQ(4, chip.V[4]);
}
}
